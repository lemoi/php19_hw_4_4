<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

function isPOST()
{
    return $_SERVER['REQUEST_METHOD'] == 'POST';
}

function getParamPost($name)
{
    return array_key_exists($name, $_POST) ? $_POST[$name] : null;
}

function getParamGet($name)
{
    return array_key_exists($name, $_GET) ? $_GET[$name] : null;
}

function redirect($page)
{
    header("Location: $page");
    die;
}

function getPDO()
{
    $opt = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES => false,
    ];
    //$pdo = new PDO("mysql:host=localhost;charset=utf8;dbname=global", "lmoiseev", "neto1490");
    $pdo = new PDO("mysql:host=localhost;port=3306;charset=utf8;dbname=global", "root", "root", $opt);

    // эмуляция разрешит использовать многократное использование одного именнованного параметра в одном запросе, хотя возможно сделает код уязвимым к определенного рода инъекционным атакам
    //$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);

    return $pdo;
}

function createTable($name)
{
    try {
        $pdo = getPDO();
        $result = $pdo->exec("CREATE TABLE $name (`id` int)");
    } catch (Exception $e) {
        echo('Ошибка создания таблицы<br>');
        $result = false;
    }
    return $result;
}

function getTables()
{
    try {
        $pdo = getPDO();
        $entries = $pdo->query("SHOW TABLES");
        return $result = $entries->fetchAll(PDO::FETCH_COLUMN, 0);
    } catch (Exception $e) {
        echo('Ошибка добавления таблицы<br>');
        $result = false;
    }
    return $result;
}

function getFields($name)
{
    try {
        $pdo = getPDO();
        $entries = $pdo->query("DESCRIBE $name");
        return $result = $entries->fetchAll();
    } catch (Exception $e) {
        echo('Ошибка получения полей<br>');
        $result = false;
    }
    return $result;
}

function addField($table, $field, $type)
{
    try {
        $pdo = getPDO();
        $result = $pdo->exec("ALTER TABLE $table ADD $field $type");
    } catch (Exception $e) {
        echo('Ошибка добавления поля<br>');
        $result = false;
    }
    return $result;
}

function deleteField($table, $field)
{
    try {
        $pdo = getPDO();
        $result = $pdo->exec("ALTER TABLE $table DROP COLUMN $field");
    } catch (Exception $e) {
        echo('Ошибка удаления поля<br>');
        $result = false;
    }
    return $result;
}

function getField($table, $field)
{
    $fields = getFields($table);
    if ($fields != false) {
        foreach ($fields as $row) {
            if ($row['Field'] == $field) {
                return $row;
            }
        }
    }
    return false;
}

function updateField($table, $field, $newName, $newType = null)
{
    $properties = getFields($table, $field);
    if ($properties == false) {
        return false;
    }

    try {
        $pdo = getPDO();
        $type = is_null($newType) ? $properties['Type'] : $newType;
        $result = $pdo->exec("ALTER TABLE $table CHANGE $field $newName $type");
    } catch (Exception $e) {
        echo('Ошибка обновления поля<br>');
        $result = false;
    }

    return $result;
}

function deleteTable($table)
{
    try {
        $pdo = getPDO();
        $result = $pdo->exec("DROP TABLE IF EXISTS $table");
    } catch (Exception $e) {
        echo('Ошибка удаления таблиц<br>');
        $result = false;
    }
    return $result;
}