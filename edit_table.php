<?php
require_once 'lib/function.php';

$table = getParamGet('table');
$action = getParamGet('action');
$field = getParamGet('field');
$type = getParamPost('Type') == 'varchar' ? 'varchar(100)' : 'int(11)';
if (isPOST()) {
    if (getParamPost('addField')) {
        if ($action == 'edit') {
            $result = updateField($table, $field, getParamPost('Field'), $type);
        } else {
            $result = addField($table, getParamPost('Field'), $type);
        }
        if ($result !== false) {
            redirect('?table=' . $table);
        }
    }
} else {
    if ($action == 'delete') {
        $result = deleteField($table, getParamGet('field'));
        if ($result !== false) {
            redirect('?table=' . $table);
        } else {
            $field = null;
        }
    } elseif ($action == 'edit') {
        $properties = getField($table, $field);
        if ($properties !== false) {
            $type = $properties['Type'];
        }
    } elseif ($action == 'delete_table') {
        $result = deleteTable($table);
        if ($result !== false) {
            redirect('list_table.php');
        }
    }
}

?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>PHP-19. Task 4.4</title>
    <style>
        table {
            border-spacing: 0;
            border-collapse: collapse;
            margin-top: 10px;
        }

        table td, table th {
            border: 1px solid #ccc;
            padding: 5px;
        }

        table th {
            background: #eee;
        }
    </style>
</head>
<body>
<a href="index.php">Главная</a>
<a href="list_table.php">Список таблиц</a>
<a href="<?= "?table={$table}&action=delete_table" ?>">Удалить таблицу</a>
<h1>Поля таблицы <?= $table ?></h1>
<form method="POST">
    <input type="text" name="Field" placeholder="Имя поля" value="<?= $field ?>">
    <select name="Type">
        <option <?= strpos($type, 'int') === false ? '' : 'selected ' ?>value="int">Целое число</option>
        <option <?= strpos($type, 'varchar') === false ? '' : 'selected ' ?>value="varchar">Строка</option>
    </select>
    <input type="submit" name="addField" value="<?= $action === 'edit' ? 'Сохранить' : 'Добавить' ?>">
</form>
<table>
    <tr>
        <th>Имя</th>
        <th>Тип</th>
        <th>Null</th>
        <th>Ключ</th>
        <th>По умолчанию</th>
        <th>Дополнительно</th>
        <th></th>
    </tr>
    <?php
    $fields = getFields($table);
    if ($fields !== false) {
        foreach ($fields as $row) {
            ?>
            <tr>
                <td><?= $row['Field'] ?></td>
                <td><?= $row['Type'] ?></td>
                <td><?= $row['Null'] ?></td>
                <td><?= $row['Key'] ?></td>
                <td><?= is_null($row['Default']) ? 'NULL' : $row['Default'] ?></td>
                <td><?= $row['Extra'] ?></td>
                <td>
                    <a href="<?= "?table={$table}&field={$row['Field']}&action=delete" ?>">Удалить</a>
                    <a href="<?= "?table={$table}&field={$row['Field']}&action=edit" ?>">Изменить</a>
                </td>
            </tr>
            <?php
        }
    }
    ?>
</table>
</body>
</html>