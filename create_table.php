<?php

require_once 'lib/function.php';

if (isPOST()) {
    $tableName = getParamPost('name');
    if (getParamPost('create') && !empty($tableName)) {
        $result = createTable($tableName);
        if ($result !== false) {
            redirect('edit_table.php?table=' . $tableName);
        }
    }
}

?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>PHP-19. Task 4.4</title>
</head>
<body>
<a href="index.php">Главная</a>
<a href="list_table.php">Список таблиц</a>
<h1>Создание таблицы</h1>
<form method="POST">
    <input type="text" name="name" placeholder="Наименование">
    <input type="submit" name="create" value="Добавить">
</form>
</body>
</html>
